// **************************************************     EVENTS        ************************************************************************

function buttonClick(){
    document.write("Hello World");
}  
function mouseOver(){
    document.getElementById("mouseEvent").innerHTML="Mouse Is Over Me";
}
function mouseOut(){
    document.getElementById("mouseEvent").innerHTML="Mouse Is Away From Me";
}


// *********************************************************         COOKIES        *****************************************************************
    // <!-- ************** Wrting a cookie ********* -->
function writeCookie(){
    if(document.myform.customer.value=="" || document.myform.age.value==""){
        alert("Enter Some Name");
    }
    valueOfCookie= escape(document.myform.customer.value + document.myform.customer.value)+" "+":"+";";
      //using : or; or space using escape() before storing in a cookie
    document.cookie="Divu"+" "+valueOfCookie;     
                  //key-value pair stored in cookie
    document.write("Our Cookie:"+ document.cookie);
}
    // <!-- ************** Reading cookies ********* -->
    var allcookies="";
    function ReadCookie() {  
        var allcookies  =  document.cookie;  
        document.write ("All Cookies : " + allcookies ); 

        // Get all the cookies pairs in an array  
     cookiearray = allcookies.split(';');  
     
     // Now take key value pair out of this array  
     for(var i = 0; i<cookiearray.length; i++) {  
        name  =  cookiearray[i].split('=')[0];  
        value = cookiearray[i].split('=')[1];  
        document.write ("Key is : " + name + " and Value is : " + value); 
     } 
     } 
    //  <!-- ****************************** Page Redirection ******************************** -->
            
    function windowLocation(){
        window.location="https://www.tutorialspoint.com/es6/es6_page_redirect.htm";
    }
    function windowLocationHref(){
        window.location.href="https://www.tutorialspoint.com/es6/es6_page_redirect.htm";
    }
    function windowLocationReload(){
        window.location.reload("https://www.tutorialspoint.com/es6/es6_page_redirect.htm");
    }
    function windowLocationAssign(){
        window.location.assign("https://www.tutorialspoint.com/es6/es6_page_redirect.htm");
    }
    function windowLocationReplace(){
        window.location.replace("https://www.tutorialspoint.com/es6/es6_page_redirect.htm");
    }
    function windowNavigate(){
        window.navigate("https://www.tutorialspoint.com/es6/es6_page_redirect.htm");
    }
  
    // <!-- **********************************************  DIALOG BOX ************************************ -->
    function Warn(){                                    //1) alert
        alert("Please Enter a Value");        
    }


    function funcConfirm(){                                        //2) confirm
        var retVal=confirm("Do u want to proceed?");     
        if (retVal==true){
            document.write("Proceed");           
        } 
        else{
            document.write("Don't proceed");
        }   
    }


    function funcPrompt(){                                        //3) prompt
        var promptVal=prompt("Enter Your Name","Please Enter Your Name");
        if(promptVal==""){
            document.write("Nothing is entered");  
        }
        else{
            document.write("User entered:"+ " "+ promptVal);  
        }  
    }

    // **********************************************  VOID OPERATOR ****************************************************

    