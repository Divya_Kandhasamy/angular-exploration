// ****************************** Label with break ***********************************************
outerloop: // This is the label name  
 for (var i = 0; i < 5; i++) {
    console.log("Outerloop: " + i);
    innerloop: for (var j = 0; j < 5; j++) {
        if (j > 3)
            break;
        // Quit the innermost loop  
        if (i == 2)
            break innerloop;
        // Do the same thing  
        if (i == 4)
            break outerloop; // Quit the outer loop  
        console.log("Innerloop: " + j);
    }
}
//O.P:
Outerloop: 0;
Innerloop: 0;
Innerloop: 1;
Innerloop: 2;
Innerloop: 3;
Outerloop: 1;
Innerloop: 0;
Innerloop: 1;
Innerloop: 2;
Innerloop: 3;
Outerloop: 2;
Outerloop: 3;
Innerloop: 0;
Innerloop: 1;
Innerloop: 2;
Innerloop: 3;
Outerloop: 4;
// ******************************  Continue ***********************************************
outerloop: // This is the label name  
 for (var i = 0; i < 3; i++) {
    console.log("C.Outerloop: " + i);
    for (var j = 0; j < 5; j++) {
        if (j == 3) {
            continue;
        }
        console.log("C.Innerloop: " + j);
    }
    console.log("C.Outerloop: " + i);
}
//O.P:
// C.Outerloop: 0
// C.Innerloop: 0
// C.Innerloop: 1
// C.Innerloop: 2
// C.Innerloop: 4
// C.Outerloop: 1
// C.Innerloop: 0
// C.Innerloop: 1
// C.Innerloop: 2
// C.Innerloop: 4
// C.Outerloop: 2
// C.Innerloop: 0
// C.Innerloop: 1
// C.Innerloop: 2
// C.Innerloop: 4
// ******************************  Label with Continue ***********************************************
outerloop: // This is the label name  
 for (var i = 0; i < 3; i++) {
    console.log("C.L.Outerloop: " + i);
    for (var j = 0; j < 5; j++) {
        if (j == 3) {
            continue outerloop;
        }
        console.log("C.L.Innerloop: " + j);
    }
    console.log("C.L.Outerloop: " + i);
}
//O.P:
// C.Outerloop: 0
// C.Innerloop: 0
// C.Innerloop: 1
// C.Innerloop: 2
// C.Innerloop: 4
// C.Outerloop: 1
// C.Innerloop: 0
// C.Innerloop: 1
// C.Innerloop: 2
// C.Innerloop: 4
// C.Outerloop: 2
// C.Innerloop: 0
// C.Innerloop: 1
// C.Innerloop: 2
// C.Innerloop: 4
