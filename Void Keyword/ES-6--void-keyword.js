"use strict";
exports.__esModule = true;
//The void operator evaluates the given expression and then returns undefined.
//This operator allows to evaluate an expressions (that produce a value) and to return undefined(instead of the value from that expression).
void function test() {
    console.log('boo!');
};
// test();   // expected output: "boo!", but we have defined it as "void", hence the error is coming
// ******************* VOID in IIFE (Immediately Invoked Function Expression) ***************************
void function test() {
    console.log('boo!');
}();
