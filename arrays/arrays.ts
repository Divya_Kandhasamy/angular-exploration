//types of declarations:

let fruits:string[]=['Apple','Orange','Kiwi'];
// or
let veggies: Array<string>=['Carrot','Drumstick','Beetroot'];
// or multiple array

let fruitsCount:(string | number)[]=['Apple', 1, 2, 'Orange','Kiwi'];
// or
let veggiesCount: Array<string | number>=['Carrot', 1 ,2, 'Beetroot','Drumstick'];



console.log (fruits[2]);  //accessing array elements

//***************************** looping *****************************

for(var i=0; i<fruits.length; i++){
    console.log(fruits[i]);       // output: Apple Orange Kiwi
}

for(var index in fruits){     // Automatically iterate through the array. 'in' is keyword
    console.log(fruits[index]);  // output: Apple Orange Kiwi
}


//********************Array Methods*********************************

console.log ('pop: ' + fruits.pop());  //output---> kiwi //REMOVES the LAST element of the array and RETURN that element

fruits.push('Papaya');    //Adds new elements to the array and returns the new array length
console.log(fruits);        //output--->  ['Apple', 'Orange', 'Papaya']


console.log ('sort: ' + veggies.sort());     //output--->  sort: Beetroot,Carrot,Drumstick


console.log ('concat: ' + veggies.concat(["1","2"]));    //output--->  concat: Beetroot,Carrot,Drumstick,1,2


console.log ('IndexOf: ' + veggies.indexOf("Cabbage"));    //output---> -1 (no such element found)
console.log ('IndexOf: ' + veggies.indexOf("Beetroot"));    //output---> 0  (since we have sorted already)


// ******************** copyWithin ************************

//The copyWithin() method allow copies part of an array to another location in the same array and returns it WITHOUT modifying its length.

const copyArray= [ 1,  2,  3, 4, 5, 6, 7, 8 ];

// (insert position, start position, end position)
console.log(copyArray.copyWithin(3, 1, 3));      // output: Uint8Array [1, 2, 3, 2, 3, 6, 7, 8]
console.log(copyArray.copyWithin(3, 0))   //end posiiton is optional, if not given, it will take upto the end of the aray // output: copyArray [1, 2, 3, 1,2, 3, 4, 5]



// ********************** fill *****************************

    const array1 = [1, 2, 3, 4];

    // fill with 0 from position 2 until position 6
    console.log(array1.fill(0, 2, 6));
    // expected output: [1, 2, 0, 0]

    // fill with 5 from position 1
    console.log(array1.fill(5, 1));
    // expected output: [1, 5, 5, 5]

    console.log(array1.fill(6));
    // expected output: [6, 6, 6, 6]






