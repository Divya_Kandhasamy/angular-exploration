//Anonymous function:  function without name
(function () {
    console.log("Anonymous");
});
// When you assign an anonymous function to a variable, it is called "Function Expression"
var func_exp = function () {
    console.log("Anonymous");
};
func_exp();
//O.P: Anonymous
//************************************** Immediately Invoked Function Expression (IIFE)*********************************
//using function expression:
var main = function () {
    var loop = function () {
        for (var x = 0; x < 5; x++) {
            console.log(x);
        }
    }();
};
main();
// O.P:
// 0
// 1
// 2
// 3
// 4
//Using Anonymous function:
var main = function () {
    (function () {
        for (var x = 0; x < 5; x++) {
            console.log(x);
        }
    })();
};
main();
// O.P:
// 0
// 1
// 2
// 3
// 4
//Note: We didn't make a call to "loop" function, but it got exected, because of the usage of '()' at the end of loop function.This is called as IIFE.
