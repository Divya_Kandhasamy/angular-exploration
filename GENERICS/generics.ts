// Generics provide a way to make components work with any data type and not restrict to one data type. So, components can be called or used with a variety of data types.


function myArr<T>(items:T[]):T[]{
    return new Array<T>().concat(items);
}

let myNumArr=myArr<number>([100,200]);    //we declared it as a number array during function call
let myStrArr=myArr<string>(["HI","Hello"]);   //we declared it as a string array during function call

myNumArr = myArr([100, 200]); // Declaring like this also OK. It will automatically assign the type as number.. so u cant push string into this array
myStrArr = myArr(["HI", "Hello"]); // Declaring like this also OK. It will automatically assign the type as string.. so u cant push number into this array 

myNumArr.push(300);
myStrArr.push("Hi");

console.log(myNumArr);   //O.P: [ 100, 200, 300 ]
console.log(myStrArr);   //O.P: [ 'HI', 'Hello', 'Hi' ]

// myNumArr.push("Hi"); // Compiler Error
// myStrArr.push(300); // Compiler Error


 //****************************************       Multiple Type Variables         ***********************************************

 function displayType<T, U, D>(id:T, name:U, age:D, section:string): void {                    //U , D are user-defined variables
    console.log(typeof(id) + ", " + typeof(name)+ ", " + typeof(age));  
  }
  
  displayType<number, string, number>(1, "Steve", 12, "A"); // O.P: number, string, number   
                                                            // we have declared non-generic data-type "section".. It is also acceptable with generic datatypes.
                                                            // We dont need to assign the type during funtcion call, since it is already decalred as string


 // ************************************************       Generic Constraints         *****************************************************

 class Person {
    firstName: string;
    lastName: string;

    constructor(fname:string,  lname:string) { 
        this.firstName = fname;
        this.lastName = lname;
    }
}

function display<T extends Person>(per: T): void {
    console.log(`${ per.firstName} ${per.lastName}` );
}
var per = new Person("Bill", "Gates");
display(per); //Output: Bill Gates

//display("Bill", "Gates");//Compiler Error



///In the above example, the display function is a generic function with constraints. A constraint is specified after the generic type in the angle brackets. The constraint <T extends Person> specifies that the generic type T must extend the class Person. So, the Person class or any other class that extends the Person class can be set as generic type while calling the display function, otherwise the compiler will give an error.