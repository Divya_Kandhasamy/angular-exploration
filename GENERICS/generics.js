// Generics provide a way to make components work with any data type and not restrict to one data type. So, components can be called or used with a variety of data types.
function myArr(items) {
    return new Array().concat(items);
}
var myNumArr = myArr([100, 200]); //we declared it as a number array during function call
var myStrArr = myArr(["HI", "Hello"]); //we declared it as a string array during function call
myNumArr = myArr([100, 200]); // Declaring like this also OK. It will automatically assign the type as number.. so u cant push string into this array
myStrArr = myArr(["HI", "Hello"]); // Declaring like this also OK. It will automatically assign the type as string.. so u cant push number into this array 
myNumArr.push(300);
myStrArr.push("Hi");
console.log(myNumArr); //O.P: [ 100, 200, 300 ]
console.log(myStrArr); //O.P: [ 'HI', 'Hello', 'Hi' ]
// myNumArr.push("Hi"); // Compiler Error
// myStrArr.push(300); // Compiler Error
//****************************************       Multiple Type Variables         ***********************************************
function displayType(id, name, age, section) {
    console.log(typeof (id) + ", " + typeof (name) + ", " + typeof (age));
}
displayType(1, "Steve", 12, "A"); // O.P: number, string, number   
// we have declared non-generic data-type "section".. It is also acceptable with generic datatypes.
// We dont need to assign the type during funtcion call, since it is already decalred as string
// ************************************************       Generic Constraints         *****************************************************
var Person = /** @class */ (function () {
    function Person(fname, lname) {
        this.firstName = fname;
        this.lastName = lname;
    }
    return Person;
}());
function display(per) {
    console.log(per.firstName + " " + per.lastName);
}
var per = new Person("Bill", "Gates");
display(per); //Output: Bill Gates
//display("Bill", "Gates");//Compiler Error
