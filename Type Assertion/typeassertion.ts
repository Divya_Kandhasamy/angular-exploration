let x:any = "divu";
var s:any;
 s = <string>x;
console.log(typeof (s));  //Output: string

//another method
let a:any = 1;
let b = a as number;
console.log(typeof (b));   //Output: number

// e.g:

let code: any = 123; 
let employeeCode = <number> code; 
console.log(typeof(employeeCode)); //Output: number


/* In the above example, we have a variable code of type any. 
  We assign the value of this variable to another variable called employeeCode.
  However, we know that code is of type number, even though it has been declared as 'any'.
  So, while assigning code to employeeCode, we have asserted that code is of type number in this case, and we are certain about it. 
  Now, the type of employeeCode is number. */





//   Example: Type Assertion with Object
 let employee = { };
 employee.name = "John";   //Compiler Error: Property 'name' does not exist on type '{}'
 employee.code = 123;      //Compiler Error: Property 'code' does not exist on type '{}'




// solution:
interface Employee { 
    name: string; 
    code: number; 
} 

let employee = <Employee> { }; 
employee.name = "John"; // OK
employee.code = 123; // OK

console.log(employee.name);