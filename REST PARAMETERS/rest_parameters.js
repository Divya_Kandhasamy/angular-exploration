function greet(greeting) {

    console.log(arguments)
    var names = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        names[_i - 1] = arguments[_i];
    }
    return greeting + " " + names.join(",") + "!";
}
greet("Hi", "Mohan", "Das");
greet("Hi", "Mohan", "Das", "Hi", "Mohan", "Das");
